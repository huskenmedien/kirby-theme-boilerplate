const path = require("path");
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

let config = {
  entry: './src/index.js',
  source: {
    path: './src/',
    entry: 'index.js'
  },
  output: {
    path: './public',
    scripts: 'scripts.js',
    styles: 'styles.css',
  },
  devtool: false
};


module.exports = (env, argv) => {

  if (argv.mode !== 'production') {
    config.devtool = 'source-map';
  }

  return {
    mode: argv.mode, // 'production'
    devtool: config.devtool,
    entry: path.resolve(__dirname, config.source.path + config.source.entry),
    output: {
      path: path.resolve(__dirname, config.output.path),
      filename: config.output.scripts, //'[name].js'
    },
    resolve: {
      extensions: ['*', '.js', '.jsx']
    },
    plugins: [new MiniCssExtractPlugin({
      filename: config.output.styles,
    })],
    module: {
      rules: [
        {
          test: /\.(s(a|c)ss)$/,
          use: [MiniCssExtractPlugin.loader,'css-loader', 'sass-loader']
        }
      ],
    }
  };

};
